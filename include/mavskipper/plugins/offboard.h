/* This file contains Skipper utilities for offboard control
 * of a MAV. It is capable of directing the MAV to goto a position in
 * local NED space, directly control yaw, pitch and roll, and more.
 *
 * Note that offboard commands will not work if the drone is not set
 * in offboard mode (Offboard for PX4, Guided for ArduPilot),
 * or does not support the SET_POSITION_TARGET_LOCAL_NED
 * and SET_ATTITUDE_TARGET MAVLink messages.
 */
#ifndef __MAVSKIPPER_OFFBOARD_H
#define __MAVSKIPPER_OFFBOARD_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "common/mavlink_msg_command_int.h"
#include "common/mavlink_msg_command_long.h"
#include "common/mavlink_msg_command_ack.h"
#include "common/mavlink_msg_command_cancel.h"
#include "common/common.h"

#include "mavskipper/services/command.h"
#include "mavskipper/process_mavlink.h"

#define HAVE_ENUM_NAV_CMD
#define HAVE_ENUM_POSITION_TARGET_TYPEMASK

/* Send local NED position target command.
 * This position is relative to the takeoff position
 * of the drone.
 * Position units are in meters.
 * Velocity units are in meters per second.
 * Acceleration units are in meters per second per second.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target system id
 * @param target_compid: target component id
 * @param north: distance north from starting location, in meters
 * @param east: distance east from starting location, in meters
 * @param down: distance down from starting location, in meters
 * @param north_m_s: velocity northwards (m/s)
 * @param east_m_s: velocity eastwards (m/s)
 * @param down_m_s: velocity downwards (m/s)
 * @param north_m_ss: accel northwards (m/s)
 * @param east_m_ss: accel eastwards (m/s)
 * @param down_m_ss: accel downwards (m/s)
 * @param ignore_dim: bitmask of which dimensions to ignore (e.g. yaw)
 * @param ack_cb: callback for COMMAND_ACK (result); can be NULL
 */
int skipper_set_offboard_ned_position(skipper_conn* conn,
        uint8_t target_sysid, uint8_t target_compid,
        double north, double east, double down,
        double north_m_s, double east_m_s, double down_m_s,
        double north_m_ss, double east_m_ss, double down_m_ss,
        uint16_t ignore_dim,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send attitude (rotation) target command.
 * Roll, pitch, yaw are all in radians.
 * Rollrate, pitchrate, yawrate are all in radians per second.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target system id
 * @param target_compid: target component id
 * @param roll: roll
 * @param pitch: pitch
 * @param yaw: yaw
 * @param ack_cb: callback for COMMAND_ACK (result); can be NULL
 */
int skipper_set_attitude_target(skipper_conn* conn,
        uint8_t target_sysid, uint8_t target_compid,
        double roll, double pitch, double yaw,
        double rollrate, double pitchrate, double yawrate,
        uint16_t ignore_dim,
        void (*ack_cb)(mavlink_command_ack_t msg));

#endif // __MAVSKIPPER_OFFBOARD_H
