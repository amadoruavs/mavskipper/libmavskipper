#!/bin/sh
# This script builds a commonly usable CMakeLists/Arduino-framework compatible library
# distribution.
DIR=$(dirname $0)
rm -rf ${DIR}/mavskipper_lib/
mkdir ${DIR}/mavskipper_lib/
cp ${DIR}/**/*.c  ${DIR}/mavskipper_lib
cp -r ${DIR}/modules/mavlink/common  ${DIR}/mavskipper_lib
cp -r ${DIR}/include/mavskipper/*  ${DIR}/mavskipper_lib

cat <<EOT >> ${DIR}/mavskipper_lib/CMakeLists.txt
# To compile this library with a CMake project, simply add_subdirectory(<path_to_mavskipper_lib>)
cmake_minimum_required(VERSION 3.12)

# file(GLOB MAVSKIPPER_HEADER_LIST CONFIGURE_DEPENDS ./*.h ./*.hpp)
file(GLOB SRCS ./*.c)
add_library(modern_library \${SRCS})
target_include_directories(modern_library PUBLIC ./)
EOT
