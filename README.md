# libmavskipper

A very simple, but high level C library for controlling MAVLink-based drones and other vehicles.

## Usage

There are two ways to use this library:
- The "proper" way on a clean-slate project is to include this project as a Meson
subproject. See the examples.
- The most likely way you will use this is to include it using the generated Arduino/CMake compatible
library.
    - For most cases, the prebuilt `mavskipper_lib` directory will contain a working Arduino-compatible
    library that can be included in a sketch directory or a PlatformIO `lib` directory. It also includes
    a CMakeLists.txt which allows `mavskipper_lib` to be included as a subdirectory in any CMake-based build
    system, such as Zephyr RTOS.
    - If the library is modified, you can regenerate `mavskipper_lib` by running the `./build_arduino_lib.sh` script.
    - Note that you do not need to modify the library to add new functionality. See below.

## Where does this sit in the Dronecode ecosystem?

It is important to note that *libmavskipper is NOT intended as a direct competitor to MAVSDK*. Although it can
perform some of the same high-level actions, it instead aims to expose a simpler API that allows more extensibility
and control over programming messages. It also aims for a "hybrid" implementation, which is device-agnostic
(in other words, it can run on both a GCS/server and on a receiving embedded device).

## Accessibility

libmavskipper aims to give you access to low-level control of MAVLink
messages, while also providing higher-level wrappers to abstract
the more boring details. The library is organized into 3 levels:
- Base (`process_mavlink.h`)
- Services (`services/`)
- Plugins (`plugins/`)

As well as `utils/` for support functions, such as reading from UDP.

The Base level provides the low-level `skipper_sub` and `skipper_conn`
structs, as well as the base utilities for subscribing to a message,
processing a message, and store a callback for publishing a message.

The Services level provides generally direct 1-to-1 operations for
MAVLink microservices; for instance, the Heartbeat Protocol and Command
Protocol are implemented on this layer (e.g. send_heartbeat, command_long_subscribe).

The Plugins level is the highest level of abstraction offered by the
library, and implements specific functionality on top of the microservices.
This is usually used for commands; for instance, `action.h` implements "action"
commands, such as arm/disarm and takeoff. This is most analogous to the
high-level API offered by MAVSDK.

## Extending the Libary

libmavskipper's architecture allows easy extension to support new message sets
and protocols. For instance, say we want to implement a custom trajectory upload protocol.
We can simply implement a new Service-level file which offers `subscribe_trajectory_info`,
`send_trajectory_ack`, `request_trajectory_int`, `subscribe_trajectory_int`. 

But how do we store state across messages?

### The Context API

The low-level `base` API can register any function as a subscriber callback for a
certain message ID. When a subscriber is created, it is created with a `void *context` pointer,
which can be used to point to any context object. This object can be used to track state across
receiving multiple messages.

This pattern is very flexible in a variety of use cases. For instance:
- In `command.h`, sending a command automatically creates a subscriber to
`COMMAND_ACK`, with `ack_callback` in the context. This allows the callback to be
passed through and called when `COMMAND_ACK` is received.
- Also in `command.h`, the implementation for processing `COMMAND_INT` and `COMMAND_LONG`
uses a `command_context` struct to store callbacks for the desired command ID,
then use a function to pre-filter the correct command id and
target system/component.
- In `mission.h`, `skipper_mission_item_send (unimplemented)` uses a context
object to store an array of mission items to be returned to a callback when
all mission items are transferred.

Using this pattern, it should be easy to implement stateful microservice message flows.
