# command_server

This example shows how mavskipper can be used to implement
device-side servers. In this case we're implementing a simple
version the v2 Gimbal Protocol.

This can be tested using the accompanying pymavlink script.

Note that pymavlink will claim that the GIMBAL_MANAGER_INFORMATION
message is an invalid message ID (280). This is because Pymavlink appears
to be based on an outdated fork of the main
MAVLink message set, and their fork does not have the gimbal manager
messages.

The pymavlink script runs on 14580 and the example mavskipper program
runs on 14550.
