# Script for testing command_server.
# Requires pymavlink.
from pymavlink import mavutil
import time

master = mavutil.mavlink_connection("udpin:0.0.0.0:14580")
master.wait_heartbeat()
print("got heartbeat", master.target_system, master.target_component)

orig = time.time()
send = True

while True:
    try:
        print(master.recv_match().to_dict())
    except AttributeError:
        pass

    if time.time() - orig > 1:
        print("Sending request")
        master.mav.command_long_send(1, 154,
                                     512, 0, 280, 0, 0, 0, 0, 0, 0)
        orig = time.time()
