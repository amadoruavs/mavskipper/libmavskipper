/* command_server
 *
 * This example connects to UDP port 14550 and subscribes to command
 * messages. This case implements a basic version of the Gimbal protocol.
 */
#include <math.h>
#include <stdint.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <common/mavlink.h>
#include "mavskipper/process_mavlink.h"
#include "mavskipper/plugins/action.h"
#include "mavskipper/services/heartbeat.h"
#include "mavskipper/services/command.h"
#include "mavskipper/util/udp.h"

skipper_conn mav_conn;
struct skipper_udp_connection conn;

// Callback for REQUEST_MESSAGE.
void request_msg_callback(mavlink_command_long_t command,
        uint8_t src_sysid, uint8_t src_compid)
{
    printf("Received req for gimbal info\n");
    if (command.param1 == MAVLINK_MSG_ID_GIMBAL_MANAGER_INFORMATION)
    {
        // Send ACK first
        mavlink_command_ack_t ack = {
            .command = command.command,
            .result = MAV_RESULT_ACCEPTED
        };
        skipper_command_ack_send(&mav_conn, ack);

        // Publish some fake info
        // This might not work on some libraries if they
        // actually validate
        mavlink_gimbal_manager_information_t info = {
            .time_boot_ms = 0,
            .gimbal_device_id = 1,
            .roll_min = -1,
            .roll_max = 1,
            .pitch_min = -1,
            .pitch_max = 1,
            .yaw_min = -1,
            .yaw_max = 1,
        };
        mavlink_message_t msg;
        mavlink_msg_gimbal_manager_information_encode(
                mav_conn.sysid, mav_conn.compid,
                &msg, &info);
        skipper_udp_send_msg(&conn, msg);
    }

    // Normally you'd implement the rest of it but we won't
    // bother. This is enough for testing
}

void send_message(mavlink_message_t msg)
{
    skipper_udp_send_msg(&conn, msg);
}

void process_message(mavlink_message_t msg)
{
    printf("received %d from %d %d\n", msg.msgid, msg.sysid, msg.compid);
    skipper_process_message(&mav_conn, msg);
}

int main(int argc, char* argv[])
{
    // Declare host and port to use for UDP.
    char* host = "0.0.0.0";
    int port = 14550;

    // Initialize UDP.
    // Set up process_message callback for UDP connection
    skipper_udp_init(&conn, host, port, process_message);
    skipper_udp_bind_socket(&conn);

    // Initialize MAVLink connection. Note how the sysid and compid can be overriden.
    // We need to manually add the message destination to send to; normally libmavskipper
    // auto-registers destinations but since the test script doesn't send hearbeats
    // we need to set it up manually
    skipper_conn_init(&mav_conn, send_message);
    skipper_udp_add_destination(&conn, host, 14580);
    mav_conn.sysid = 1;
    mav_conn.compid = MAV_COMP_ID_GIMBAL;
    mav_conn.type = MAV_TYPE_GIMBAL;

    // Set up command subscriber
    skipper_command_long_subscribe(&mav_conn, MAV_CMD_REQUEST_MESSAGE,
            request_msg_callback);

    // Keep track of time
    time_t now;
    time_t start;
    time_t last_heartbeat;
    time(&last_heartbeat);
    time(&start);

    printf("Initialized!\n");
    // heartbeat_publish(&mav_conn);

    while (true)
    {
        // Receive message from UDP
        skipper_udp_recv_msg(&conn);
        time(&now);

        // Publish heartbeat about every 2Hz
        if (now - last_heartbeat > 0.5)
        {
            printf("publish hearbeat\n");
            skipper_heartbeat_publish(&mav_conn);

            last_heartbeat = now;
        }
    }
}
