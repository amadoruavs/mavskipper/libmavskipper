/* telemetry
 *
 * This example connects to drone (sysid: 1, compid: 1) on
 * port udp://:14550 (simulator port), makes it takeoff, flies around for
 * a while, and reads various telemetry such as position and rotation
 * while it is in the air.
 */
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <common/mavlink.h>
#include "mavskipper/process_mavlink.h"
#include "mavskipper/plugins/action.h"
#include "mavskipper/services/heartbeat.h"
#include "mavskipper/services/command.h"
#include "mavskipper/util/udp.h"

// Create a skipper connection object (represents a MAVLink node on
// a network)
skipper_conn mav_conn;

// Create a UDP connection object from utility
struct skipper_udp_connection conn;

// Declare position callback, will be called when we receive
// MAVLINK_MSG_GLOBAL_POSITION_INT messages.
void position_callback(mavlink_message_t msg, void* context)
{
    mavlink_global_position_int_t pos;
    mavlink_msg_global_position_int_decode(&msg, &pos);
    printf("%d position (%f, %f, %f)\n",
            msg.sysid,
            ((double) pos.lat / 1e9), ((double) pos.lon) / 1e9, pos.alt / 1000.0);
}

// Declare local position callback, will be called when we receive
// MAVLINK_MSG_LOCAL_POSITION_NED messages
void local_position_callback(mavlink_message_t msg, void* context)
{
    mavlink_local_position_ned pos;
    mavlink_msg_local_position_ned_decode(&msg, &pos);
    printf("%d local position (%f, %f, %f) velocity (%f %f %f)\n",
            msg.sysid,
            msg.x, msg.y, msg.z,
            msg.vx, msg.vy, msg.vz);
}

// Declare callback for attitude
void attitude_callback(mavlink_message_t msg, void* context)
{
    mavlink_attitude att;
    mavlink_msg_attitude_decode(&msg, &att);
    printf("%d rotation (%f, %f, %f)\n",
            msg.sysid,
            msg.roll * 180 / PI, msg.pitch * 180 / PI, msg.yaw * 180 / PI);
}

void send_message(mavlink_message_t msg) { skipper_udp_send_msg(&conn, msg); }
void process_message(mavlink_message_t msg) { skipper_process_message(&mav_conn, msg); }

int main(int argc, char* argv[])
{
    // Declare host and port to use for UDP.
    char* host = "0.0.0.0";
    int port = 14550;

    // Initialize UDP.
    skipper_udp_init(&conn, host, port, process_message);
    skipper_udp_bind_socket(&conn);

    // Initialize MAVLink connection.
    skipper_conn_init(&mav_conn, send_message);

    // Add subscriber for messages from drone at 1, 1.
    // Setting sysid and compid to 0 will receive from all nodes.
    skipper_sub gpos_sub = {
        .msg_id = MAVLINK_MSG_ID_GLOBAL_POSITION_INT,
        .src_sysid = 1,
        .src_compid = 1,
        .data_callback = position_callback
    };
    skipper_subscribe(&mav_conn, lpos_sub);
    skipper_sub lpos_sub = {
        .msg_id = MAVLINK_MSG_ID_LOCAL_POSITION_NED,
        .src_sysid = 1,
        .src_compid = 1,
        .data_callback = local_position_callback
    };
    skipper_subscribe(&mav_conn, lpos_sub);
    skipper_sub apos_sub = {
        .msg_id = MAVLINK_MSG_ID_ATTITUDE,
        .src_sysid = 1,
        .src_compid = 1,
        .data_callback = attitude_callback
    };
    skipper_subscribe(&mav_conn, apos_sub);

    // Keep track of time
    time_t now;
    time_t start;
    time_t last_heartbeat;
    time(&last_heartbeat);
    time(&start);
    bool sent_takeoff = false;

    while (true)
    {
        skipper_udp_recv_msg(&conn);
        time(&now);
        if (now - last_heartbeat > 0.5)
        {
            printf("publish hearbeat\n");
            skipper_heartbeat_publish(&mav_conn);

            last_heartbeat = now;
        }
        if (now - start > 1)
        {
            if (!sent_takeoff)
            {
                printf("send arm\n");
                skipper_arm(&mav_conn, 1, 1, NULL);
                skipper_takeoff(&mav_conn, 1, 1, 10, 0, 0, NULL);
                sent_takeoff = true;
            }
        }

        // Land 20 seconds after start
        if (now - start > 20)
        {
            printf("landing\n");
            skipper_land(&mav_conn, 1, 1, NULL);
            break;
        }
    }
}
