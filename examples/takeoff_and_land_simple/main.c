/* takeoff_and_land
 *
 * This example connects to drone (sysid: 1, compid: 1) on
 * port udp://:14550 (simulator port), makes it takeoff, then
 * makes it land again.
 */
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <common/mavlink.h>
#include "mavskipper/process_mavlink.h"
#include "mavskipper/plugins/action.h"
#include "mavskipper/services/heartbeat.h"
#include "mavskipper/services/command.h"
#include "mavskipper/plugins/action.h"
#include "mavskipper/util/udp.h"

skipper_conn mav_conn;
struct skipper_udp_connection conn;

void send_message(mavlink_message_t msg) { skipper_udp_send_msg(&conn, msg); }
void process_message(mavlink_message_t msg) { skipper_process_message(&mav_conn, msg); }

int main(int argc, char* argv[])
{
    // Declare host and port to use for UDP.
    char* host = "0.0.0.0";
    int port = 14550;

    // Initialize UDP.
    skipper_udp_init(&conn, host, port, process_message);
    skipper_udp_bind_socket(&conn);

    // Initialize MAVLink connection.
    skipper_conn_init(&mav_conn, send_message);

    // Keep track of time
    time_t now;
    time_t start;
    time_t last_heartbeat;
    time(&last_heartbeat);
    time(&start);
    bool sent_takeoff = false;

    while (true)
    {
        skipper_udp_recv_msg(&conn);
        time(&now);
        if (now - last_heartbeat > 0.5)
        {
            printf("publish hearbeat\n");
            skipper_heartbeat_publish(&mav_conn);

            last_heartbeat = now;
        }
        if (now - start > 1)
        {
            if (!sent_takeoff)
            {
                printf("send arm\n");
                skipper_arm(&mav_conn, 1, 1, NULL);
                skipper_takeoff(&mav_conn, 1, 1, 10, 0, 0, NULL);
                sent_takeoff = true;
            }
        }

        // Land 20 seconds after start
        if (now - start > 20)
        {
            printf("landing\n");
            skipper_land(&mav_conn, 1, 1, NULL);
            break;
        }
    }
}
