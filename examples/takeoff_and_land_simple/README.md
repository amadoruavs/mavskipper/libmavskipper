# takeoff_and_land

This example arms the drone, sends a TAKEOFF command, and
lands. It also provides a telemetry readout of the global
position of the drone.
