/* takeoff_and_land
 *
 * This example connects to drone (sysid: 1, compid: 1) on
 * port udp://:14550 (simulator port), makes it takeoff, then
 * makes it land again.
 */
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <common/mavlink.h>
#include "mavskipper/process_mavlink.h"
#include "mavskipper/plugins/action.h"
#include "mavskipper/services/heartbeat.h"
#include "mavskipper/services/command.h"
#include "mavskipper/util/udp.h"

// Create a skipper connection object (represents a MAVLink node on
// a network)
skipper_conn mav_conn;

// Create a UDP connection object from utility
struct skipper_udp_connection conn;

// Declare position callback, will be called when we receive
// MAVLINK_MSG_GLOBAL_POSITION_INT messages.
void position_callback(mavlink_message_t msg, void* context)
{
    mavlink_global_position_int_t pos;
    mavlink_msg_global_position_int_decode(&msg, &pos);
    printf("%d position (%f, %f, %f)\n",
            msg.sysid,
            ((double) pos.lat / 1e9), ((double) pos.lon) / 1e9, pos.alt / 1000.0);
}

// Declare a callback for when we receive ACKs from commands.
// Since we just want to debug print the result all commands are attached to this
void ack_callback(mavlink_command_ack_t ack)
{
    printf("Command %d acknowledged, result %d\n",
            ack.command, ack.result);
}

// Declare publish callback for skipper_conn to use. Here we're just wrapping
// the skipper UDP utility.
void send_message(mavlink_message_t msg)
{
    skipper_udp_send_msg(&conn, msg);
}

// Declare callback to process mavlink messages.
void process_message(mavlink_message_t msg)
{
    skipper_process_message(&mav_conn, msg);
}

int main(int argc, char* argv[])
{
    // Declare host and port to use for UDP.
    char* host = "0.0.0.0";
    int port = 14550;

    // Initialize UDP.
    skipper_udp_init(&conn, host, port, process_message);
    skipper_udp_bind_socket(&conn);

    // Initialize MAVLink connection. Note how the sysid and compid can be overriden.
    skipper_conn_init(&mav_conn, send_message);
    mav_conn.sysid = 242;
    mav_conn.compid = MAV_TYPE_GCS;

    // Add subscriber for position messages from drone at 1, 1.
    // Setting sysid and compid to 0 will receive from all nodes.
    skipper_sub pos1_sub = {
        .msg_id = MAVLINK_MSG_ID_GLOBAL_POSITION_INT,
        .src_sysid = 1,
        .src_compid = 1,
        .data_callback = position_callback
    };
    skipper_subscribe(&mav_conn, pos1_sub);

    // Create the structs for sending arm and takeoff commands
    // See (https://mavlink.io/en/messages/common.html#MAV_CMD_COMPONENT_ARM_DISARM)
    mavlink_command_long_t arm_cmd = {
        .command = MAV_CMD_COMPONENT_ARM_DISARM,
        .target_system = 1,
        .target_component = 1,
        .param1 = 1.0f,
        .param2 = 21196
    };

    // See (https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_TAKEOFF)
    mavlink_command_long_t takeoff_cmd = {
        .command = MAV_CMD_NAV_TAKEOFF,
        .target_system = 1,
        .target_component = 1,
        .param5 = NAN, // lat
        .param6 = NAN, // lon
        .param7 = 10 // altitude (only works on ArduPilot)
    };

    // See (https://mavlink.io/en/messages/common.html#MAV_CMD_NAV_LAND)
    mavlink_command_long_t land_cmd = {
        .command = MAV_CMD_NAV_LAND,
        .target_system = 1,
        .target_component = 1,
        .param5 = NAN,
        .param6 = NAN
    };

    // Keep track of time
    time_t now;
    time_t start;
    time_t last_heartbeat;
    time(&last_heartbeat);
    time(&start);

    // Only send takeoff once
    bool sent_takeoff = false;

    while (true)
    {
        // Receive message from UDP (blocks until new message)
        // On message receive, it will call the registered callback
        skipper_udp_recv_msg(&conn);

        time(&now);

        // Publish heartbeat about every 2Hz
        if (now - last_heartbeat > 0.5)
        {
            printf("publish hearbeat\n");
            skipper_heartbeat_publish(&mav_conn);

            last_heartbeat = now;
        }

        // Publish takeoff 1 second after program start
        // Note that the drone must be receiving heartbeats
        // to takeoff, otherwise the failsafe will kick in
        // and disarm it
        if (now - start > 1)
        {
            if (!sent_takeoff)
            {
                printf("send arm\n");
                skipper_command_long_send(&mav_conn, arm_cmd, ack_callback);
                skipper_command_long_send(&mav_conn, takeoff_cmd, ack_callback);
                sent_takeoff = true;
            }
        }

        // Land 20 seconds after start
        if (now - start > 20)
        {
            printf("landing\n");
            skipper_command_long_send(&mav_conn, land_cmd, ack_callback);
            break;
        }
    }
}
