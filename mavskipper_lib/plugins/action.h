/* This file contains Skipper utilities for basic control
 * of a MAV. It is capable of directing the MAV to disarm, arm,
 * takeoff, land, go to a GPS location,
 * hold, and land.
 *
 * It is loosely based off of the similar MAVSDK Action plugin, and
 * intended to provide an easier to use, higher level API.
 */
#ifndef __MAVSKIPPER_ACTION_H
#define __MAVSKIPPER_ACTION_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "common/mavlink_msg_command_int.h"
#include "common/mavlink_msg_command_long.h"
#include "common/mavlink_msg_command_ack.h"
#include "common/mavlink_msg_command_cancel.h"
#include "common/common.h"

#include "mavskipper/services/command.h"
#include "mavskipper/process_mavlink.h"

#define HAVE_ENUM_NAV_CMD

/* Send arm command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target system id
 * @param target_compid: target component id
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_arm(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send disarm command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target system id
 * @param target_compid: target component id
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_disarm(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send takeoff command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * NOTE: Unlike MAVSDK, libmavskipper's altitude parameter
 * ONLY HAS EFFECT on ArduPilot (as the library sticks much closer
 * to the bare MAVLink messages). To set the takeoff altitude you must
 * write the value to the TAKEOFF_ALT_PARAM parameter using the Parameter
 * API.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target drone sysid
 * @param target_compid: target drone compid
 * @param takeoff_height: takeoff height, in meters
 * @param pitch: takeoff pitch (deg) for fixed wings. VTOL/copter should set to 0.
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_takeoff(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        double takeoff_height, double yaw, double pitch,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send land command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * TODO: Support Landing Target protocol (precision landings)
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target drone sysid
 * @param target_compid: target drone compid
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_land(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send return to launch command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * TODO: Support Landing Target protocol (precision landings)
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target drone sysid
 * @param target_compid: target drone compid
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_return_to_launch(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg));

/* Send GPS goto command.
 * Add an optional callback for ACK, or set to NULL.
 * Note that if set to NULL there will be no feedback
 * on whether the command succeeded.
 *
 * @param conn: skipper connection struct
 * @param target_sysid: target system id
 * @param target_compid: target component id
 * @param lat: latitude to go to
 * @param lon: longitude to go to
 * @param alt: altitude to go to
 * @param ack_cb: callback for COMMAND_ACK (result)
 */
int skipper_goto_gps(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        double lat, double lon, double alt,
        void (*ack_cb)(mavlink_command_ack_t msg));

#endif // __MAVSKIPPER_ACTION_H
