/* quaternion.h: a couple of quaternion utilities */
#ifndef __QUATERNION_H
#define __QUATERNION_H

void skipper_quat_to_euler(float *q, float *e);
void skipper_euler_to_quat(float *e, float *q);

#endif
