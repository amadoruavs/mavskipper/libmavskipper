#ifndef __MAVSKIPPER_PROCESS_H
#define __MAVSKIPPER_PROCESS_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

typedef void (*skipper_data_callback)(mavlink_message_t msg, void* context);

typedef struct skipper_sub {
    uint8_t src_sysid;
    uint8_t src_compid;
    int msg_id;
    skipper_data_callback data_callback;
    void* context;
} skipper_sub;

typedef struct skipper_conn {
    uint8_t sysid;
    uint8_t compid;
    uint8_t status;
    uint8_t type;
    void (*publish_callback)(mavlink_message_t msg);
    cvector_vector_type(skipper_sub) subscriptions;
} skipper_conn;

/* Search subscribers attached to a connection.
 * Filters by message ID, and optionally the sys and component ID.
 * (See https://mavlink.io/en/guide/routing.html).
 *
 * @param conn: Connection to search.
 * @param src_sysid: Source system ID. Set to 0 for all.
 * @param src_compid: Source compnent ID. Set to 0 for all.
 * @param msg_id: Message ID to subscribe to.
 */
cvector_vector_type(skipper_sub) search_subscribers(skipper_conn* conn,
        uint8_t src_sysid, uint8_t src_compid, uint32_t msg_id);

void skipper_subscribe(skipper_conn* conn, skipper_sub sub);

void skipper_conn_init(skipper_conn* conn, void (*publish_callback)(mavlink_message_t msg));

void skipper_process_message(skipper_conn* conn, mavlink_message_t msg);

#endif // __MAVSKIPPER_PROCESS_H
