#ifndef __SKIPPER_UDP_H
#define __SKIPPER_UDP_H

#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "mavskipper/cvector.h"
#include "common/mavlink.h"
#include "mavlink_types.h"

#define BUFFER_LENGTH 2041

struct skipper_udp_connection {
    // Self explanatory
    char *host;
    int port;

    // File descriptor for UDP socket, and other such variables
    int sock;
    struct sockaddr_in addr;
    socklen_t fromlen;

    // Process message callback
    void (*process_callback)(mavlink_message_t msg);

    // Recv buffer
    uint8_t buf[BUFFER_LENGTH];

    // We need to keep a vector of remote addresses
    // to reply to
    cvector_vector_type(struct sockaddr_in) remote_addrs;
};

void skipper_udp_init(struct skipper_udp_connection *conn,
        char *host, int port, void (*process_callback)(mavlink_message_t msg));

int skipper_udp_bind_socket(struct skipper_udp_connection *conn);

int skipper_udp_connect_socket(struct skipper_udp_connection *conn);

void skipper_udp_add_destination(struct skipper_udp_connection *conn,
        char* host, int port);

int skipper_udp_send_bytes(struct skipper_udp_connection *conn, uint8_t *buf, int buflen);

int skipper_udp_send_msg(struct skipper_udp_connection *conn, mavlink_message_t msg);

void skipper_udp_recv_msg(struct skipper_udp_connection *conn);

#endif // __SKIPPER_UDP_H
