#include <stddef.h>
#include <stdio.h>

#include "common/mavlink.h"
#include "mavskipper/cvector.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "mavskipper/process_mavlink.h"

cvector_vector_type(skipper_sub) search_subscribers(skipper_conn* conn,
        uint8_t src_sysid, uint8_t src_compid, uint32_t msg_id)
{
    cvector_vector_type(skipper_sub) matches = NULL;
    for (size_t i = 0; i < cvector_size(conn->subscriptions); ++i)
    {
        skipper_sub sub = conn->subscriptions[i];
        if (sub.msg_id == msg_id)
        {
            // If sub sysid filter is turned on, filter other messages
            // Messages with source sysid/compid 0 should be read by
            // all devices
            if (sub.src_sysid && src_sysid && sub.src_sysid != src_sysid) continue;
            if (sub.src_compid && src_compid && sub.src_compid != src_compid) continue;

            cvector_push_back(matches, sub);
        }
    }

    return matches;
}

void skipper_subscribe(skipper_conn* conn, skipper_sub sub)
{
    cvector_push_back(conn->subscriptions, sub);
}

void skipper_conn_init(skipper_conn* conn, void (*publish_callback)(mavlink_message_t msg))
{
    conn->subscriptions = NULL;
    conn->publish_callback = publish_callback;
    if (conn->sysid == 0)
    {
        conn->sysid = 242;
    }

    if (conn->compid == 0)
    {
        conn->compid = MAV_COMP_ID_MISSIONPLANNER;
    }

    if (conn->type == 0)
    {
        conn->type = MAV_TYPE_GCS;
    }

    if (conn->status == 0)
    {
        conn->status = MAV_STATE_ACTIVE;
    }
}

void skipper_process_message(skipper_conn* conn, mavlink_message_t msg)
{
    cvector_vector_type(skipper_sub) matches = search_subscribers(
            conn, msg.sysid, msg.compid, msg.msgid);
    for (size_t i = 0; i < cvector_size(matches); ++i)
    {
        matches[i].data_callback(msg, matches[i].context);
    }
}
