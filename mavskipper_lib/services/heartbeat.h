#ifndef __MAVSKIPPER_HEARTBEAT_H
#define __MAVSKIPPER_HEARTBEAT_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "minimal/mavlink_msg_heartbeat.h"
#include "mavskipper/process_mavlink.h"

/* Publish a heartbeat message.
 * (See https://mavlink.io/en/messages/common.html#HEARTBEAT).
 * Uses the configured sysid and compid from the skipper connection struct.
 *
 * Note that libmavskipper has no concept of timeouts or time in general,
 * so regular publishing is to be implemented by the user. This function
 * should be called at least once per second.
 *
 * @param conn: Connection to publish from.
 */
void skipper_heartbeat_publish(skipper_conn* conn);

/* Publish a raw heartbeat message. Allows full control over
 * the data fields of the message, rather than assuming
 * the program is a GCS.
 *
 * @param conn: Connection to publish from.
 * @param heartbeat: Heartbeat message to publish.
 */
void skipper_heartbeat_publish_raw(skipper_conn* conn, mavlink_heartbeat_t heartbeat);

#endif // __MAVSKIPPER_HEARTBEAT_H
