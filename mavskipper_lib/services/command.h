/* This file contains the Skipper utilities necessary to
 * correctly implement the MAVLink Command Protocol
 * (https://mavlink.io/en/services/command.html).
 *
 * This service header is primarily intended for systems/devices
 * implementing the command API. For simply sending commands, the
 * basic publish/subscribe utilities should work, but there is a
 * subscribe_command_{int, long} function defined here which abstracts
 * that.
 *
 * Devices implementing the Command Protocol can
 * subscribe to messages through the regular subscription API,
 * and send ACKs using functions defined here.
 */
#ifndef __MAVSKIPPER_COMMAND_H
#define __MAVSKIPPER_COMMAND_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "common/mavlink_msg_command_int.h"
#include "common/mavlink_msg_command_long.h"
#include "common/mavlink_msg_command_ack.h"
#include "common/mavlink_msg_command_cancel.h"
#include "mavskipper/process_mavlink.h"

typedef void (*ack_callback_t)(mavlink_command_ack_t ack_msg);
typedef void (*command_int_callback_t)(
        mavlink_command_int_t cmd, uint8_t src_sysid, uint8_t src_compid);
typedef void (*command_long_callback_t)(
        mavlink_command_long_t cmd, uint8_t src_sysid, uint8_t src_compid);


/* Publish a COMMAND_INT and setup callback for ACK message.
 * Uses the configured sysid and compid from the skipper connection struct.
 *
 * Note that libmavskipper has no concept of timeouts or time in general,
 * so command timeouts are to be implemented by the user.
 *
 * @param conn: Connection to publish command from.
 * @param mavlink_message_t: A packed MAVLink COMMAND_INT or COMMAND_LONG.
 * @param ack_callback: Callback for when ACK is received (command progress or finish).
 */
void skipper_command_int_send(skipper_conn* conn, mavlink_command_int_t command,
        ack_callback_t ack_callback);

/* Same as command_int_send, but COMMAND_LONG.
 */
void skipper_command_long_send(skipper_conn* conn, mavlink_command_long_t command,
        ack_callback_t ack_callback);

/* Cancel a long-running command. WIP.
 * (See https://mavlink.io/en/messages/common.html#COMMAND_CANCEL.)
 *
 * @param conn: Connection to publish from.
 * @param command_id: ID of command to cancel.
 */
void skipper_command_cancel(skipper_conn* conn, uint16_t command_id);

/* Send a COMMAND_ACK. This can be used by devices to implement
 * commands.
 */
void skipper_command_ack_send(skipper_conn* conn, mavlink_command_ack_t command);

/* Subscribe to a COMMAND_INT. This can be used by devices to implement
 * certain protocols.
 * (See Command Protocol link above.)
 *
 * ACK replies should be sent manually, as libmavskipper cannot
 * make any assumptions about runtime.
 *
 * @param conn: Connection to publish from.
 * @param command_id: ID of command to sub to.
 */
void skipper_command_int_subscribe(skipper_conn* conn, uint16_t command_id,
        command_int_callback_t callback);

/* Subscribe to a COMMAND_LONG. This can be used by devices to implement
 * certain protocols.
 * (See Command Protocol link above.)
 *
 * ACK replies should be sent manually, as libmavskipper cannot
 * make any assumptions about runtime.
 *
 * @param conn: Connection to publish from.
 * @param command_id: ID of command to cancel.
 */
void skipper_command_long_subscribe(skipper_conn* conn, uint16_t command_id,
        command_long_callback_t callback);


#endif // __MAVSKIPPER_COMMAND_H
