# To compile this library with a CMake project, simply add_subdirectory(<path_to_mavskipper_lib>)
cmake_minimum_required(VERSION 3.12)

# file(GLOB MAVSKIPPER_HEADER_LIST CONFIGURE_DEPENDS ./*.h ./*.hpp)
file(GLOB SRCS ./*.c)
add_library(modern_library ${SRCS})
target_include_directories(modern_library PUBLIC ./)
