/* action.c
 *
 * Implements common autopilot actions, such as taking off,
 * landing, arming etc.
 */
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "common/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "minimal/mavlink_msg_heartbeat.h"

#include "mavskipper/process_mavlink.h"
#include "mavskipper/services/heartbeat.h"
#include "mavskipper/services/command.h"

#include "mavskipper/plugins/action.h"

int skipper_arm(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_COMPONENT_ARM_DISARM,
        .param1 = 1
    };

    printf("ARMINGYETE\n");
    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}

int skipper_disarm(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_COMPONENT_ARM_DISARM,
        .param1 = 0
    };
    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}

int skipper_takeoff(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        double takeoff_height, double yaw, double pitch,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_NAV_TAKEOFF,
        .param1 = pitch,
        .param4 = yaw,
        .param5 = NAN,
        .param6 = NAN,
        .param7 = takeoff_height
    };

    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}

int skipper_land(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_NAV_LAND,
        .param5 = NAN,
        .param6 = NAN
    };

    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}

int skipper_return_to_launch(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_NAV_RETURN_TO_LAUNCH,
    };

    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}

int skipper_goto_gps(skipper_conn* conn, uint8_t target_sysid, uint8_t target_compid,
        double lat, double lon, double alt,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_command_long_t cmd = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .command = MAV_CMD_DO_REPOSITION,
        .param1 = -1,
        .param2 = MAV_DO_REPOSITION_FLAGS_CHANGE_MODE,
        .param4 = NAN,
        .param5 = lat,
        .param6 = lon,
        .param7 = alt
    };

    skipper_command_long_send(conn, cmd, ack_cb);

    return 1;
}
