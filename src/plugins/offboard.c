/* This file contains Skipper utilities for offboard control
 * of a MAV. It is capable of directing the MAV to goto a position in
 * local NED space, directly control yaw, pitch and roll, and more.
 *
 * Note that offboard commands will not work if the drone is not set
 * in offboard mode (Offboard for PX4, Guided for ArduPilot),
 * or does not support the SET_POSITION_TARGET_LOCAL_NED
 * and SET_ATTITUDE_TARGET MAVLink messages.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "common/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "common/mavlink_msg_command_int.h"
#include "common/mavlink_msg_command_long.h"
#include "common/mavlink_msg_command_ack.h"
#include "common/mavlink_msg_command_cancel.h"

#include "mavskipper/services/command.h"
#include "mavskipper/quaternion.h"
#include "mavskipper/process_mavlink.h"

#include "mavskipper/plugins/offboard.h"

#define HAVE_ENUM_NAV_CMD
#define HAVE_ENUM_POSITION_TARGET_TYPEMASK

int skipper_set_offboard_ned_position(skipper_conn* conn,
        uint8_t target_sysid, uint8_t target_compid,
        double north, double east, double down,
        double north_m_s, double east_m_s, double down_m_s,
        double north_m_ss, double east_m_ss, double down_m_ss,
        uint16_t ignore_dim,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    mavlink_set_position_target_local_ned_t target = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .coordinate_frame = MAV_FRAME_LOCAL_NED,
        .type_mask = ignore_dim,
        .x = north,
        .y = east,
        .z = down,
        .vx = north_m_s,
        .vy = east_m_s,
        .vz = down_m_s,
        .afx = north_m_ss,
        .afy = east_m_ss,
        .afz = down_m_ss
    };

    mavlink_message_t target_msg;
    mavlink_msg_set_position_target_local_ned_encode(conn->sysid, conn->compid,
            &target_msg, &target);

    if (conn->publish_callback) conn->publish_callback(target_msg);

    return 1;
}

int skipper_set_attitude_target(skipper_conn* conn,
        uint8_t target_sysid, uint8_t target_compid,
        double roll, double pitch, double yaw,
        double rollrate, double pitchrate, double yawrate,
        uint16_t ignore_dim,
        void (*ack_cb)(mavlink_command_ack_t msg))
{
    float attitude_q[4];
    float attitude_euler[] = { roll, pitch, yaw };
    skipper_euler_to_quat(attitude_euler, attitude_q);

    mavlink_set_attitude_target_t target = {
        .target_system = target_sysid,
        .target_component = target_compid,
        .type_mask = ignore_dim,
        .body_yaw_rate = yawrate,
        .body_pitch_rate = pitchrate,
        .body_roll_rate = rollrate,
    };
    target.q[0] = attitude_q[0];
    target.q[1] = attitude_q[1];
    target.q[2] = attitude_q[2];
    target.q[3] = attitude_q[3];

    mavlink_message_t target_msg;
    mavlink_msg_set_attitude_target_encode(conn->sysid, conn->compid,
            &target_msg, &target);

    if (conn->publish_callback) conn->publish_callback(target_msg);

    return 1;
}
