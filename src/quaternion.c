#include <math.h>
#include "mavskipper/quaternion.h"

void skipper_quat_to_euler(float *q, float *e)
{
	float roll, pitch, yaw;

	/* roll  (y-axis rotation) */
	float sinr_cosp = 2 * (q[0] * q[1] + q[2] * q[3]);
	float cosr_cosp = 1 - 2 * (q[1] * q[1] + q[2] * q[2]);
	roll = atan2f(sinr_cosp, cosr_cosp);

	/* pitch (x-axis rotation) */
	float sinp = 2 * (q[0] * q[2] - q[3] * q[1]);
	if (fabsf(sinp) >= 1) {
		pitch = (sinp >= 0) ? M_PI / 2 : -M_PI / 2;
	} else {
		pitch = asinf(sinp);
	}

	/* yaw (z-axis rotation) */
	float siny_cosp = 2 * (q[0] * q[3] + q[1] * q[2]);
	float cosy_cosp = 1 - 2 * (q[2] * q[2] + q[3] * q[3]);
	yaw = atan2f(siny_cosp, cosy_cosp);

	e[0] = roll * 180 / M_PI;
	e[1] = pitch * 180 / M_PI;
	e[2] = yaw * 180 / M_PI;
}

void skipper_euler_to_quat(float *e, float *q)
{
    // Get roll, pitch and yaw in radians
    float roll = e[0] * M_PI / 180;
    float pitch = e[1] * M_PI / 180;
    float yaw = e[2] * M_PI / 180;

    // Convert
    float qx = sin(roll/2) * cos(pitch/2) * cos(yaw/2) - cos(roll/2) * sin(pitch/2) * sin(yaw/2);
    float qy = cos(roll/2) * sin(pitch/2) * cos(yaw/2) + sin(roll/2) * cos(pitch/2) * sin(yaw/2);
    float qz = cos(roll/2) * cos(pitch/2) * sin(yaw/2) - sin(roll/2) * sin(pitch/2) * cos(yaw/2);
    float qw = cos(roll/2) * cos(pitch/2) * cos(yaw/2) + sin(roll/2) * sin(pitch/2) * sin(yaw/2);

    q[0] = qw;
    q[1] = qx;
    q[2] = qy;
    q[3] = qz;
}
