/* This file contains the Skipper utilities necessary to
 * correctly implement the MAVLink Command Protocol
 * (https://mavlink.io/en/services/command.html).
 *
 * This service header is primarily intended for systems/devices
 * implementing the command API. For simply sending commands, the
 * basic publish/subscribe utilities should work, but there is a
 * subscribe_command_{int, long} function defined here which abstracts
 * that.
 *
 * Devices implementing the Command Protocol can
 * subscribe to messages through the regular subscription API,
 * and send ACKs using functions defined here.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "common/mavlink_msg_command_int.h"
#include "common/mavlink_msg_command_long.h"
#include "common/mavlink_msg_command_ack.h"
#include "common/mavlink_msg_command_cancel.h"
#include "mavskipper/process_mavlink.h"

#include "mavskipper/services/command.h"

struct command_context {
    union Callback {
        command_int_callback_t int_cb;
        command_long_callback_t long_cb;
    } callback;
    uint16_t command_id;
    uint8_t sysid;
    uint8_t compid;
};

void skipper_process_ack_msg(mavlink_message_t ack_msg, void* context)
{
    ack_callback_t ack_cb = (ack_callback_t) context;
    mavlink_command_ack_t ack;
    mavlink_msg_command_ack_decode(&ack_msg, &ack);

    if (ack_cb) ack_cb(ack);
}

void skipper_process_command_msg(mavlink_message_t msg, void* context)
{
    struct command_context* cmd_context = (struct command_context*) context;
    if (cmd_context->callback.int_cb)
    {
        mavlink_command_int_t command_int;
        mavlink_msg_command_int_decode(&msg, &command_int);

        if (command_int.target_system != cmd_context->sysid) return;
        if (command_int.target_component != cmd_context->compid) return;
        if (command_int.command == cmd_context->command_id)
        {
            cmd_context->callback.int_cb(command_int, msg.sysid, msg.compid);
        }
    } else if (cmd_context->callback.long_cb)
    {
        mavlink_command_long_t command_long;
        mavlink_msg_command_long_decode(&msg, &command_long);

        if (command_long.target_system != cmd_context->sysid) return;
        if (command_long.target_component != cmd_context->compid) return;
        if (command_long.command == cmd_context->command_id)
        {
            cmd_context->callback.long_cb(command_long, msg.sysid, msg.compid);
        }
    }
}

void skipper_command_int_send(skipper_conn* conn, mavlink_command_int_t command,
        ack_callback_t ack_callback)
{
    mavlink_message_t msg;
    mavlink_msg_command_int_encode(conn->sysid, conn->compid, &msg, &command);
    if (conn->publish_callback) conn->publish_callback(msg);

    skipper_sub ack_sub = {
        .msg_id = MAVLINK_MSG_ID_COMMAND_ACK,
        .src_sysid = command.target_system,
        .src_compid = command.target_component,
        .data_callback = skipper_process_ack_msg,
        .context = ack_callback
    };
    skipper_subscribe(conn, ack_sub);
}

void skipper_command_long_send(skipper_conn* conn, mavlink_command_long_t command,
        ack_callback_t ack_callback)
{
    mavlink_message_t msg;
    mavlink_msg_command_long_encode(conn->sysid, conn->compid, &msg, &command);
    if (conn->publish_callback) conn->publish_callback(msg);

    skipper_sub ack_sub = {
        .msg_id = MAVLINK_MSG_ID_COMMAND_ACK,
        .src_sysid = command.target_system,
        .src_compid = command.target_component,
        .data_callback = skipper_process_ack_msg,
        .context = ack_callback
    };
    skipper_subscribe(conn, ack_sub);
}

void skipper_command_ack_send(skipper_conn* conn, mavlink_command_ack_t command)
{
    mavlink_message_t msg;
    mavlink_msg_command_ack_encode(conn->sysid, conn->compid, &msg, &command);
    if (conn->publish_callback) conn->publish_callback(msg);
}

void command_cancel(skipper_conn* conn, uint16_t command_id)
{
    /* NOT IMPLEMENTED */
}

void skipper_command_int_subscribe(skipper_conn* conn, uint16_t command_id,
        command_int_callback_t callback)
{
    struct command_context* context = malloc(sizeof(struct command_context));
    context->command_id = command_id;
    context->callback.int_cb = callback;
    context->sysid = conn->sysid;
    context->compid = conn->compid;

    skipper_sub command_int_sub = {
        .msg_id = MAVLINK_MSG_ID_COMMAND_INT,
        .src_sysid = 0,
        .src_compid = 0,
        .data_callback = skipper_process_command_msg,
        .context = context
    };
    skipper_subscribe(conn, command_int_sub);
}

void skipper_command_long_subscribe(skipper_conn* conn, uint16_t command_id,
        command_long_callback_t callback)
{
    struct command_context* context = malloc(sizeof(struct command_context));
    context->command_id = command_id;
    context->callback.long_cb = callback;
    context->sysid = conn->sysid;
    context->compid = conn->compid;

    skipper_sub command_long_sub = {
        .msg_id = MAVLINK_MSG_ID_COMMAND_LONG,
        .src_sysid = 0,
        .src_compid = 0,
        .data_callback = skipper_process_command_msg,
        .context = context
    };
    skipper_subscribe(conn, command_long_sub);
}
