#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "mavskipper/cvector.h"
#include "minimal/mavlink.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "minimal/mavlink_msg_heartbeat.h"
#include "mavskipper/process_mavlink.h"

#include "mavskipper/services/heartbeat.h"

void skipper_heartbeat_publish(skipper_conn* conn)
{
    mavlink_heartbeat_t heartbeat = {
        .type = conn->type,
        .mavlink_version = 2,
        .autopilot = MAV_AUTOPILOT_INVALID,
        .base_mode = MAV_MODE_FLAG_AUTO_ENABLED,
        .system_status = conn->status
    };
    mavlink_message_t msg;
    mavlink_msg_heartbeat_encode(conn->sysid, conn->compid, &msg, &heartbeat);

    if (conn->publish_callback) conn->publish_callback(msg);
}

void skipper_heartbeat_publish_raw(skipper_conn* conn, mavlink_heartbeat_t heartbeat)
{
    mavlink_message_t msg;
    mavlink_msg_heartbeat_encode(conn->sysid, conn->compid, &msg, &heartbeat);
    if (conn->publish_callback) conn->publish_callback(msg);
}
