/* Create a UDP-based MAVLink connection.
 * Basically just a glorified wrapper that feeds
 * UDP data straight into the processor.
 */
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <arpa/inet.h>

#include "common/mavlink.h"
#include "mavskipper/cvector.h"
#include "mavlink_types.h"
#include "mavlink_helpers.h"

#include "mavskipper/util/udp.h"

#define MSG_NOBLOCK 0x01
#define BUFFER_LENGTH 2041

void skipper_udp_init(struct skipper_udp_connection *conn,
        char *host, int port, void (*process_callback)(mavlink_message_t msg))
{
    conn->host = host;
    conn->port = port;
    conn->process_callback= process_callback;

    memset(&(conn->addr), 0, sizeof(conn->addr));
    conn->addr.sin_family = AF_INET;
    conn->addr.sin_addr.s_addr = inet_addr(host);
    conn->addr.sin_port = htons(port);
    conn->sock = socket(AF_INET, SOCK_DGRAM, 0);
    int enable = 1;
    if (setsockopt(conn->sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        printf("setsockopt(SO_REUSEADDR) failed\n");

    struct timeval read_timeout;
    read_timeout.tv_sec = 0;
    read_timeout.tv_usec = 10;
    setsockopt(conn->sock, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout));

    conn->fromlen = sizeof(conn->addr);

    conn->remote_addrs = NULL;
}

int skipper_udp_bind_socket(struct skipper_udp_connection *conn)
{
    return bind(conn->sock, (struct sockaddr*)&(conn->addr), sizeof(struct sockaddr));
}

int skipper_udp_connect_socket(struct skipper_udp_connection *conn)
{
    return connect(conn->sock, (struct sockaddr*)&(conn->addr), sizeof(struct sockaddr));
}

bool skipper_udp_remote_connection_exists(
        struct skipper_udp_connection *conn, struct sockaddr_in* addr)
{
    for (size_t i = 0; i < cvector_size(conn->remote_addrs); ++i)
    {
        if (conn->remote_addrs[i].sin_addr.s_addr == addr->sin_addr.s_addr &&
                conn->remote_addrs[i].sin_port == addr->sin_port)
            return true;
    }
    return false;
}

void skipper_udp_add_destination(struct skipper_udp_connection *conn,
        char* host, int port)
{
    struct sockaddr_in dest_addr;

    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_addr.s_addr = inet_addr(host);
    dest_addr.sin_port = htons(port);

    cvector_push_back(conn->remote_addrs, dest_addr);
}

int skipper_udp_send_bytes(struct skipper_udp_connection *conn, uint8_t *buf, int buflen)
{
    // When we send, just send back to every
    // stored remote address.
    for (size_t i = 0; i < cvector_size(conn->remote_addrs); ++i)
    {
        struct sockaddr_in dest_addr = conn->remote_addrs[i];
        size_t len = sizeof(struct sockaddr_in);
        sendto(conn->sock, buf, buflen, 0,
                (struct sockaddr*)&dest_addr, len);
    }

    return 0;
}

int skipper_udp_send_msg(struct skipper_udp_connection *conn, mavlink_message_t msg)
{
    uint8_t sendbuf[BUFFER_LENGTH];
    int buffer_len = mavlink_msg_to_send_buffer(sendbuf, &msg);

    return skipper_udp_send_bytes(conn, sendbuf, buffer_len);
}

void skipper_udp_recv_msg(struct skipper_udp_connection *conn)
{
    ssize_t recsize;
    struct sockaddr_in src_addr;
    socklen_t src_addr_len = sizeof(src_addr);

    memset(conn->buf, 0, BUFFER_LENGTH);
    recsize = recvfrom(conn->sock, (void*) conn->buf, BUFFER_LENGTH, MSG_NOBLOCK, (struct sockaddr*)&src_addr, &src_addr_len);
    if (recsize > 0)
    {
        // something received - parse packet
        mavlink_message_t msg;
        mavlink_status_t status;

        for (int i = 0; i < recsize; ++i)
        {
            if (mavlink_parse_char(MAVLINK_COMM_0, conn->buf[i], &msg, &status))
            {
                // packet received
                // Add the client to our list of remote addresses
                if (!skipper_udp_remote_connection_exists(conn, &src_addr))
                {
                    cvector_push_back(conn->remote_addrs, src_addr);
                }

                if (conn->process_callback) conn->process_callback(msg);
            }
        }
    }
}
